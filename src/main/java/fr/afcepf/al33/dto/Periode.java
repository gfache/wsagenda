package fr.afcepf.al33.dto;

import java.io.Serializable;
import java.util.Date;

public class Periode implements Serializable {
	private static final long serialVersionUID = 1L; // nécessaire si utilisation avec RMI
    private Date dateDebut;
    private Date dateFin;
        
	public Periode() {
		super();
	}

	public Periode(Date dateDebut, Date dateFin) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}
	
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

}
