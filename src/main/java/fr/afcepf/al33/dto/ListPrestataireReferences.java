package fr.afcepf.al33.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListPrestataireReferences implements Serializable  
{
	private static final long serialVersionUID = 1L;
	
	private List<PrestataireReference> prestatairesReferences;

	public List<PrestataireReference> getPrestatairesReferences() {
		return prestatairesReferences;
	}

	public void setPrestatairesReferences(List<PrestataireReference> prestatairesReferences) {
		this.prestatairesReferences = prestatairesReferences;
	}

	public ListPrestataireReferences(List<PrestataireReference> prestatairesReferences) {
		super();
		this.prestatairesReferences = prestatairesReferences;
	}

	public ListPrestataireReferences() {
		super();
		this.prestatairesReferences = new ArrayList<PrestataireReference>();
	}
	
	
}
