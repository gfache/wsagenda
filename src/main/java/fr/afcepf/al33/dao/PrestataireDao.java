package fr.afcepf.al33.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al33.entity.CalendarEvent;
import fr.afcepf.al33.entity.Prestataire;

public interface PrestataireDao extends CrudRepository<Prestataire, Integer> {

}
