package fr.afcepf.al33;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class WSAgendaAPP {

    public static void main(String[] args) {
		SpringApplication app = new SpringApplication(WSAgendaAPP.class);
		app.setAdditionalProfiles("web.dev");
		ConfigurableApplicationContext context = app.run(args);

		System.out.println("http://localhost:9890/WSAgenda/index.html");
    }
    
}
